import numbers
from fastapi import status, Response, APIRouter
import psycopg
from pydantic import BaseModel
from .categories import CategoryOut


router = APIRouter()


class ErrorMessage(BaseModel):
    message: str


class ClueOut(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: CategoryOut
    canon: bool



@router.get(
    "/api/clues/{clue_id}",
    response_model=ClueOut,
    responses={404:{"model": ErrorMessage}},)
def get_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT clues.id, clues.answer, clues.question, clues.value, clues.invalid_count, 
                categories.id, categories.title, categories.canon, clues.canon
                FROM clues 
                INNER JOIN categories ON (clues.category_id = categories.id)
                WHERE clues.id = %s;
                """,
                [clue_id],
            )
            result = cur.fetchone()
            if result is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Clue not found"}

            return {
                "id": result[0],
                "answer": result[1],
                "question": result[2],
                "value": result[3],
                "invalid_count": result[4],
                "category": {
                    "id": result[5],
                    "title": result[6],
                    "canon": result[7],
                },
                "canon": result[8],
            }

@router.get("/api/random-clue", response_model=ClueOut,
responses={404: {"model": ErrorMessage}},)
def random_clue(valid: bool):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute("""
                SELECT clues.id, clues.answer, clues.question, clues.value, clues.invalid_count, 
                categories.id, categories.title, categories.canon, clues.canon
                FROM clues 
                INNER JOIN categories ON (clues.category_id = categories.id)
                WHERE clues.id = %s
                WHERE clues.invalid_count = 0
                ORDER BY RANDOM() LIMIT 1
            """,[valid])
            result = cur.fetchone()
            if valid is True:
                return {
                    "id": result[0],
                    "answer": result[1],
                    "question": result[2],
                    "value": result[3],
                    "invalid_count": result[4],
                    "category": {
                        "id": result[5],
                        "title": result[6],
                        "canon": result[7],
                    },
                    "canon": result[8],
                }


@router.delete(
    "/api/clues/{clue_id}",
    response_model=ErrorMessage,
    responses={400: {"model": ErrorMessage}},
)
def remove_clue(clue_id: int, response: Response):
        with psycopg.connect() as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute(
                        """
                        DELETE FROM clues
                        WHERE id = %s;
                        """,
                        [clue_id],
                    )
                    return {
                        "message": "Success",
                    }
                except psycopg.errors.ForeignKeyViolation:
                    response.status_code = status.HTTP_400_BAD_REQUEST
                    return {
                        "message": "Cannot delete clue",
                    }